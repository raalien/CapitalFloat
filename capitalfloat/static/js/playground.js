$(function() {
    $("#upload-file").click(function() {
        input_file = $("#data-file").val();
        var data = new FormData();
        data.append("file", $("#data-file")[0].files[0]);
        $.ajax({
            method: 'POST',
            url: '/file/info',
            data: data,
            contentType: false,
            cache : false,
            processData: false,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRFToken", $("input[name=csrfmiddlewaretoken]").val());
            },
            success: function(resp) {
                $("#show-file-info").html(resp);
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});


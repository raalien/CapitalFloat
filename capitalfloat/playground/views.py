from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
import json
from datetime import datetime
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
from django.db import transaction

# Create your views here.

@csrf_exempt
def home(request):
    return render_to_response('playground/home.html', 
            {}, context_instance = RequestContext(request))

@csrf_exempt
def fileinfo(request):
    file = request.FILES.get('file')
    lines = file.readlines()
    wordcount = 0
    linecount = 0
    charcount = 0
    for line in lines:
        linecount += 1
        for word in line.split(' '):
            wordcount += 1
            charcount += len(word.strip())

    return render_to_response('playground/file-info-table.html',
        {'linecount': linecount,
        'wordcount': wordcount,
        'charcount': charcount},
        content_type='application/html')

@csrf_exempt
def musician_api(request, musician_id):
    from models import Musician, Album
    context = {}
    if request.method=='POST':
        body = json.loads(request.body.strip())
        data = {}
        if body.get('fname'):
            data['first_name'] = body.get('fname')
        if body.get('lname'):
            data['last_name'] = body.get('lname')
        if body.get('instrument'):
            data['instrument'] = body.get('instr')
        if body.get('id'):
            data['musician_id'] = body.get('mus_id');
        if data:
            Musician.objects.update_or_create(id=data.get('musicianid'), defaults=data)
            context['msg'] = 'successfully executed'
        else:
            context['msg'] = 'information missing'

    if request.method=='GET':
        try:
            o = Musician.objects.get(id=musician_id)
            context['first_name'] = o.first_name
            context['last_name'] = o.last_name
            context['instrument'] = o.instrument
            context['msg'] = 'successfully executed'
        except:
            context['msg'] = 'id not exist'

    if request.method=='DELETE':
        try:
            body = json.loads(request.body.strip())
            with transaction.atomic():
                Album.objects.filter(musician_id=body.get('musician_id')).delete()
                Musician.objects.filter(id=body.get('musician_id')).delete()
                context['msg'] = 'successfully executed'
        except:
            context['msg'] = 'error occured'

    return HttpResponse(json.dumps(context), 
        content_type='application/json')

@csrf_exempt
def album_api(request, musician_id, album_id):
    from models import Musician, Album
    context = {}
    if request.method == 'POST':   
        try:
            body = json.loads(request.body)
            data = {}
            data['musician_id'] = body.get('musician_id')
            if body.get('album_id'):
                data['id'] = body.get('album_id')
            if body.get('name'):
                data['name'] = body.get('name')
            if body.get('rating'):
                data['num_stars'] = body.get('rating')
            data['release_date'] = datetime.now().date();
            Album.objects.update_or_create(id=data.get('id'), 
                musician_id=data['musician_id'], defaults=data)
            context['msg'] = 'successfully executed'
        except Exception, e:
            print e
            context['msg'] = 'incomplete information'

    if request.method == 'GET':
        context["albums"] = [album.to_json() for album in Album.objects.filter(id=album_id,
            musician_id=musician_id)]

    if request.method == 'DELETE':
        try:
            body = json.loads(request.body.strip())
            with transaction.atomic():
                Album.objects.filter(id=body.get('album_id')).delete()
                context['msg'] = 'successfully executed'
        except:
            context['msg'] = 'error occured'

    return HttpResponse(json.dumps(context),
        content_type='application/json')

@csrf_exempt
def album_api(request, musician_id):
    from models import Musician, Album
    context = {}
    if request.method == 'GET':
        context["albums"] = [album.to_json() for album in Album.objects.filter(musician_id=musician_id)]

    return HttpResponse(json.dumps(context),
        content_type='application/json')



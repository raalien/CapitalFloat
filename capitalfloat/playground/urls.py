from django.conf.urls import url
from . import views as playground_views
urlpatterns = [
        url(r'^file/info', playground_views.fileinfo),
        url(r'musician/(?P<musician_id>\d*)', playground_views.musician_api),
        url(r'album/(?P<musician_id>\d*)/(?P<album_id>\d*)', playground_views.album_api),
        url(r'album/(?P<musician_id>\d*)', playground_views.album_api),
        url(r'', playground_views.home),
    ]
from django.db import models

# Create your models here.

class Musician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    instrument = models.CharField(max_length=100)

class Album(models.Model):
    musician = models.ForeignKey(Musician, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    release_date = models.DateField()
    num_stars = models.IntegerField()

    def to_json(self):
    	return {
    		'musician_id': self.musician_id,
    		'musician_name': self.musician.first_name + ' ' + self.musician.last_name,
    		'musician_instrument': self.musician.instrument,
    		'album_name': self.name,
    		'album_rating': self.num_stars,
    		'album_release_date': self.release_date.strftime('%d-%m-%Y')
    	}

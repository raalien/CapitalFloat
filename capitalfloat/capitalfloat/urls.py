from django.conf.urls import include, url
from django.contrib import admin
from playground import urls as playground_urls

urlpatterns = [
    # Examples:
    # url(r'^$', 'capitalfloat.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'', include(playground_urls))
]
